From 54f42e1b75dd795f9d53dce181397f114eeae21b Mon Sep 17 00:00:00 2001
From: Stephan Bergmann <sbergman@redhat.com>
Date: Tue, 28 Apr 2020 16:43:31 +0200
Subject: [PATCH] Restructure solenv/bin/assemble-flatpak.sh

...so that parts of it can be reused by the Flatpak build done from Fedora RPMs
(see <https://src.fedoraproject.org/flatpaks/libreoffice>).  The reused parts
are split out into solenv/bin/assemble-flatpak-*.sh files.

(I can't remember any specific reason why I added the <releases> section to the
Flathub org.libreoffice.LibreOffice.appdata.xml; maybe just because the file
format allows for it and it appeared easy to generate the section when writing
the file.  For the Fedora LO RPMs, none of the existing appdata.xml files
contained such a section, and generating one for the Fedora Flatpak case would
require obtaining values for those LIBO_VERSION_* variables, so I just added a
switch to solenv/bin/assemble-flatpak-appdata-step1.sh allowing not to write
that section at all.  Splitting solenv/bin/assemble-flatpak-appdata.sh in two
steps is necessary because the Fedora Flatpak case wants to replace the
screenshots in the first part of the generated
org.libreoffice.LibreOffice.appdata.xml, but not in the appended original
appdata.xml files, so needs to hook in between those two steps.)

Change-Id: Ic527f3d88ccbee85e86dad3569b8e73776adf273
---
 solenv/bin/assemble-flatpak-appdata-step1.sh |  96 +++++++++++++++++
 solenv/bin/assemble-flatpak-appdata-step2.sh |  26 +++++
 solenv/bin/assemble-flatpak-desktop.sh       |  42 ++++++++
 solenv/bin/assemble-flatpak.sh               | 102 +------------------
 4 files changed, 169 insertions(+), 97 deletions(-)
 create mode 100755 solenv/bin/assemble-flatpak-appdata-step1.sh
 create mode 100755 solenv/bin/assemble-flatpak-appdata-step2.sh
 create mode 100755 solenv/bin/assemble-flatpak-desktop.sh

diff --git a/solenv/bin/assemble-flatpak-appdata-step1.sh b/solenv/bin/assemble-flatpak-appdata-step1.sh
new file mode 100755
index 000000000000..4658dd45d018
--- /dev/null
+++ b/solenv/bin/assemble-flatpak-appdata-step1.sh
@@ -0,0 +1,96 @@
+#! /bin/bash
+#
+# This file is part of the LibreOffice project.
+#
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+#
+
+# Part of solenv/bin/assemble-flatpak.sh that is shared with a downstream mechanism of building a
+# Flatpak from a Fedora libreoffice.spec file.
+#
+# Arguments:
+# $1  pathname, ending in a slash, of the directory into which to put the target
+#     org.libreoffice.LibreOffice.appdata.xml file
+# $2  "1" if a <releases> section shall be included in the target
+#     org.libreoffice.LibreOffice.appdata.xml file, "0" if not
+
+set -e
+
+## org.libreoffice.LibreOffice.appdata.xml is manually derived from the various
+## inst/share/appdata/libreoffice-*.appdata.xml (at least recent GNOME Software
+## doesn't show more than five screenshots anyway, so restrict to one each from
+## the five libreoffice-*.appdata.xml: Writer, Calc, Impress, Draw, Base):
+cat <<\EOF >"${1?}"org.libreoffice.LibreOffice.appdata.xml
+<?xml version="1.0" encoding="UTF-8"?>
+<component type="desktop">
+ <id>org.libreoffice.LibreOffice.desktop</id>
+ <metadata_license>CC0-1.0</metadata_license>
+ <project_license>MPL-2.0</project_license>
+ <name>LibreOffice</name>
+ <summary>The LibreOffice productivity suite</summary>
+ <description>
+  <p>LibreOffice is a powerful office suite.  Its clean interface and
+  feature-rich tools help you unleash your creativity and enhance your
+  productivity.  LibreOffice includes several applications that make it the most
+  powerful Free and Open Source office suite on the market: Writer (word
+  processing), Calc (spreadsheets), Impress (presentations), Draw (vector
+  graphics and flowcharts), Base (databases), and Math (formula editing).</p>
+  <p>LibreOffice supports opening and saving into a wide variety of formats, so
+  you can easily share documents with users of other popular office suites
+  without worrying about compatibility.</p>
+ </description>
+ <url type="homepage">http://www.libreoffice.org/discover/libreoffice/</url>
+ <url type="bugtracker">https://bugs.documentfoundation.org/</url>
+ <url type="donation">https://donate.libreoffice.org/</url>
+ <url type="faq">https://wiki.documentfoundation.org/Faq</url>
+ <url type="help">http://www.libreoffice.org/get-help/documentation/</url>
+ <url type="translate">https://wiki.documentfoundation.org/Translating_LibreOffice</url>
+ <screenshots>
+  <screenshot type="default">
+   <image>https://hub.libreoffice.org/screenshots/writer-01.png</image>
+   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
+  </screenshot>
+  <screenshot>
+   <image>https://hub.libreoffice.org/screenshots/calc-02.png</image>
+   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
+  </screenshot>
+  <screenshot>
+   <image>https://hub.libreoffice.org/screenshots/impress-01.png</image>
+   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
+  </screenshot>
+  <screenshot>
+   <image>https://hub.libreoffice.org/screenshots/draw-02.png</image>
+   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
+  </screenshot>
+  <screenshot>
+   <image>https://hub.libreoffice.org/screenshots/base-02.png</image>
+   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
+  </screenshot>
+ </screenshots>
+ <developer_name>The Document Foundation</developer_name>
+ <update_contact>libreoffice_at_lists.freedesktop.org</update_contact>
+ <kudos>
+  <kudo>HiDpiIcon</kudo>
+  <kudo>HighContrast</kudo>
+  <kudo>ModernToolkit</kudo>
+  <kudo>UserDocs</kudo>
+ </kudos>
+ <content_rating type="oars-1.0"/>
+EOF
+
+if [ "${2?}" = 1 ]
+then
+ cat <<EOF >>"${1?}"org.libreoffice.LibreOffice.appdata.xml
+ <releases>
+  <release
+    version="${LIBO_VERSION_MAJOR?}.${LIBO_VERSION_MINOR?}.${LIBO_VERSION_MICRO?}.${LIBO_VERSION_PATCH?}"
+    date="$(date +%Y-%m-%d)"/>
+ </releases>
+EOF
+fi
+
+cat <<\EOF >>"${1?}"org.libreoffice.LibreOffice.appdata.xml
+</component>
+EOF
diff --git a/solenv/bin/assemble-flatpak-appdata-step2.sh b/solenv/bin/assemble-flatpak-appdata-step2.sh
new file mode 100755
index 000000000000..4f06e6b19148
--- /dev/null
+++ b/solenv/bin/assemble-flatpak-appdata-step2.sh
@@ -0,0 +1,26 @@
+#! /bin/bash
+#
+# This file is part of the LibreOffice project.
+#
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+#
+
+# Part of solenv/bin/assemble-flatpak.sh that is shared with a downstream mechanism of building a
+# Flatpak from a Fedora libreoffice.spec file.
+#
+# Arguments:
+# $1  pathname, ending in a slash, of the directory containing the source libreoffice-*.appdata.xml
+#     files
+# $2  pathname, ending in a slash, of the directory containing the target
+#     org.libreoffice.LibreOffice.appdata.xml file
+
+set -e
+
+# append the appdata for the different components
+for i in "${1?}"libreoffice-*.appdata.xml
+do
+  sed "1 d; s/<id>libreoffice-/<id>org.libreoffice.LibreOffice./" "$i" \
+    >>"${2?}"org.libreoffice.LibreOffice.appdata.xml
+done
diff --git a/solenv/bin/assemble-flatpak-desktop.sh b/solenv/bin/assemble-flatpak-desktop.sh
new file mode 100755
index 000000000000..6d06de4fbbbe
--- /dev/null
+++ b/solenv/bin/assemble-flatpak-desktop.sh
@@ -0,0 +1,42 @@
+#! /bin/bash
+#
+# This file is part of the LibreOffice project.
+#
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+#
+
+# Part of solenv/bin/assemble-flatpak.sh that is shared with a downstream mechanism of building a
+# Flatpak from a Fedora libreoffice.spec file.
+#
+# Arguments:
+# $1  pathname, ending in a slash, of the directory containing the source libreoffice-*.desktop
+#     files
+# $2  pathname, ending in a slash, of the directory into which to put the target
+#     org.libreoffice.LibreOffice.*.desktop files
+
+set -e
+
+## libreoffice-*.desktop -> org.libreoffice.LibreOffice.*.desktop:
+for i in "${1?}"libreoffice-*.desktop
+do
+ sed -e 's/^Icon=libreoffice-/Icon=org.libreoffice.LibreOffice./' "$i" \
+  >"${2?}"org.libreoffice.LibreOffice."${i#"${1?}"libreoffice-}"
+done
+mv "${2?}"org.libreoffice.LibreOffice.startcenter.desktop "${2?}"org.libreoffice.LibreOffice.desktop
+
+# Flatpak .desktop exports take precedence over system ones due to
+# the order of XDG_DATA_DIRS - re-associating text/plain seems a bit much
+sed -i "s/text\/plain;//" "${2?}"org.libreoffice.LibreOffice.writer.desktop
+
+desktop-file-edit --set-key=X-Endless-Alias --set-value=libreoffice-startcenter \
+ --set-key=X-Flatpak-RenamedFrom --set-value='libreoffice-startcenter.desktop;' \
+ "${2?}"org.libreoffice.LibreOffice.desktop
+for i in base calc draw impress math writer xsltfilter
+do
+ desktop-file-edit --set-key=X-Endless-Alias --set-value=libreoffice-"$i" \
+  --set-key=X-Flatpak-RenamedFrom \
+  --set-value="libreoffice-$i.desktop;org.libreoffice.LibreOffice-$i.desktop;" \
+  "${2?}"org.libreoffice.LibreOffice."$i".desktop
+done
diff --git a/solenv/bin/assemble-flatpak.sh b/solenv/bin/assemble-flatpak.sh
index 0738fcd59ad2..8ca5bcf08c41 100755
--- a/solenv/bin/assemble-flatpak.sh
+++ b/solenv/bin/assemble-flatpak.sh
@@ -16,30 +16,9 @@ set -e
 cp -r "${PREFIXDIR?}"/lib/libreoffice /app/
 ln -s /app/libreoffice/program/soffice /app/bin/libreoffice
 
-## libreoffice-*.desktop -> org.libreoffice.LibreOffice.*.desktop:
 mkdir -p /app/share/applications
-for i in "${PREFIXDIR?}"/share/applications/libreoffice-*.desktop
-do
- sed -e 's/^Icon=libreoffice-/Icon=org.libreoffice.LibreOffice./' "$i" \
-  >/app/share/applications/org.libreoffice.LibreOffice."${i#"${PREFIXDIR?}"/share/applications/libreoffice-}"
-done
-mv /app/share/applications/org.libreoffice.LibreOffice.startcenter.desktop \
- /app/share/applications/org.libreoffice.LibreOffice.desktop
-
-# Flatpak .desktop exports take precedence over system ones due to
-# the order of XDG_DATA_DIRS - re-associating text/plain seems a bit much
-sed -i "s/text\/plain;//" /app/share/applications/org.libreoffice.LibreOffice.writer.desktop
-
-desktop-file-edit --set-key=X-Endless-Alias --set-value=libreoffice-startcenter \
- --set-key=X-Flatpak-RenamedFrom --set-value='libreoffice-startcenter.desktop;' \
- /app/share/applications/org.libreoffice.LibreOffice.desktop
-for i in base calc draw impress math writer xsltfilter
-do
- desktop-file-edit --set-key=X-Endless-Alias --set-value=libreoffice-"$i" \
-  --set-key=X-Flatpak-RenamedFrom \
-  --set-value="libreoffice-$i.desktop;org.libreoffice.LibreOffice-$i.desktop;" \
-  /app/share/applications/org.libreoffice.LibreOffice."$i".desktop
-done
+"${SRCDIR?}"/solenv/bin/assemble-flatpak-desktop.sh "${PREFIXDIR?}"/share/applications/ \
+ /app/share/applications/
 
 ## icons/hicolor/*/apps/libreoffice-* ->
 ## icons/hicolor/*/apps/org.libreoffice.LibreOffice-*:
@@ -82,81 +61,10 @@ do
   ln -rs /app/share/runtime/locale/"${lang}"/registry/"${basename}".xcd "${i}"
 done
 
-## org.libreoffice.LibreOffice.appdata.xml is manually derived from the various
-## inst/share/appdata/libreoffice-*.appdata.xml (at least recent GNOME Software
-## doesn't show more than five screenshots anyway, so restrict to one each from
-## the five libreoffice-*.appdata.xml: Writer, Calc, Impress, Draw, Base):
 mkdir -p /app/share/appdata
-cat <<EOF >/app/share/appdata/org.libreoffice.LibreOffice.appdata.xml
-<?xml version="1.0" encoding="UTF-8"?>
-<component type="desktop">
- <id>org.libreoffice.LibreOffice.desktop</id>
- <metadata_license>CC0-1.0</metadata_license>
- <project_license>MPL-2.0</project_license>
- <name>LibreOffice</name>
- <summary>The LibreOffice productivity suite</summary>
- <description>
-  <p>LibreOffice is a powerful office suite.  Its clean interface and
-  feature-rich tools help you unleash your creativity and enhance your
-  productivity.  LibreOffice includes several applications that make it the most
-  powerful Free and Open Source office suite on the market: Writer (word
-  processing), Calc (spreadsheets), Impress (presentations), Draw (vector
-  graphics and flowcharts), Base (databases), and Math (formula editing).</p>
-  <p>LibreOffice supports opening and saving into a wide variety of formats, so
-  you can easily share documents with users of other popular office suites
-  without worrying about compatibility.</p>
- </description>
- <url type="homepage">http://www.libreoffice.org/discover/libreoffice/</url>
- <url type="bugtracker">https://bugs.documentfoundation.org/</url>
- <url type="donation">https://donate.libreoffice.org/</url>
- <url type="faq">https://wiki.documentfoundation.org/Faq</url>
- <url type="help">http://www.libreoffice.org/get-help/documentation/</url>
- <url type="translate">https://wiki.documentfoundation.org/Translating_LibreOffice</url>
- <screenshots>
-  <screenshot type="default">
-   <image>https://hub.libreoffice.org/screenshots/writer-01.png</image>
-   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
-  </screenshot>
-  <screenshot>
-   <image>https://hub.libreoffice.org/screenshots/calc-02.png</image>
-   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
-  </screenshot>
-  <screenshot>
-   <image>https://hub.libreoffice.org/screenshots/impress-01.png</image>
-   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
-  </screenshot>
-  <screenshot>
-   <image>https://hub.libreoffice.org/screenshots/draw-02.png</image>
-   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
-  </screenshot>
-  <screenshot>
-   <image>https://hub.libreoffice.org/screenshots/base-02.png</image>
-   <caption><!-- Describe this screenshot in less than ~10 words --></caption>
-  </screenshot>
- </screenshots>
- <developer_name>The Document Foundation</developer_name>
- <update_contact>libreoffice_at_lists.freedesktop.org</update_contact>
- <kudos>
-  <kudo>HiDpiIcon</kudo>
-  <kudo>HighContrast</kudo>
-  <kudo>ModernToolkit</kudo>
-  <kudo>UserDocs</kudo>
- </kudos>
- <content_rating type="oars-1.0"/>
- <releases>
-  <release
-    version="${LIBO_VERSION_MAJOR?}.${LIBO_VERSION_MINOR?}.${LIBO_VERSION_MICRO?}.${LIBO_VERSION_PATCH?}"
-    date="$(date +%Y-%m-%d)"/>
- </releases>
-</component>
-EOF
-
-# append the appdata for the different components
-for i in "${PREFIXDIR?}"/share/appdata/libreoffice-*.appdata.xml
-do
-  sed "1 d; s/<id>libreoffice-/<id>org.libreoffice.LibreOffice./" "$i" \
-    >>/app/share/appdata/org.libreoffice.LibreOffice.appdata.xml
-done
+"${SRCDIR?}"/solenv/bin/assemble-flatpak-appdata-step1.sh /app/share/appdata/ 1
+"${SRCDIR?}"/solenv/bin/assemble-flatpak-appdata-step2.sh "${PREFIXDIR?}"/share/appdata/ \
+ /app/share/appdata/
 
 ## see <https://github.com/flatpak/flatpak/blob/master/app/
 ## flatpak-builtins-build-finish.c> for further places where build-finish would
-- 
2.26.2

